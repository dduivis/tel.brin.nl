<?php

namespace App\Http\Controllers;

use App\Http\Middleware\RedirectIfAuthenticated;
use App\phone_numbers;
use Faker\Provider\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;


class PhoneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $phone_numbers = DB::table('phone_numbers')->Paginate(8);
        return view('home', compact('phone_numbers'));
    }

    public function create()
    {

    }
    public function store(Request $request)
    {
            request()->validate([
                'phone_number' =>
                [
                    'required',
                    'digits:11',
                    'starts_with:31',
                    'unique:phone_numbers',
                ],
                'name' =>
                [
                    'required',
                    'max:190',
                    'min:2',
                ],],
                [
                    'phone_number.required' => 'Telefoonnummer mag niet leeg zijn !',
                    'phone_number.digits' => 'Het telefoonnummer moet uit 11 cijfers bestaan !',
                    'phone_number.starts_with' => 'Het telefoonnummer moet beginnen met de landcode 31 !',
                    'phone_number.unique' => 'Het gekozen telefoonnummer bestaat al !',
                    'name.required' => 'Naam mag niet leeg zijn !',
                    'name.max' => 'Naam mag niet groter zijn dan 190 tekens !',
                    'name.min' => 'Naam moet minimaal 2 tekens bevatten !',
                ]
            );
            $phone = new phone_numbers();
            $phone->phone_number = Input::get('phone_number');
            $phone->name = Input::get('name');
            $phone->save();
            return Redirect::back();
    }

    public function show($request)
    {

    }

    public function edit($id)
    {
        $phone_numbers = phone_numbers::find($id);
        return view('phone_numbers.edit', compact('phone_numbers'));
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'phone_number' => [
                'required',
                'digits:11',
                'starts_with:31',
                Rule::unique('phone_numbers')->ignore($id),
        ],
            'name' => [
                'required',
                'max:190',
                'min:2',
        ],
            ],[
            'phone_number.required' => 'Telefoonnummer mag niet leeg zijn !',
            'phone_number.digits' => 'Het telefoonnummer moet uit 11 cijfers bestaan !',
            'phone_number.starts_with' => 'Het telefoonnummer moet beginnen met de landcode 31 !',
            'name.required' => 'Naam mag niet leeg zijn !',
            'name.max' => 'Naam mag niet groter zijn dan 190 tekens !',
            'name.min' => 'Naam moet minimaal 2 tekens bevatten !',
        ]
            );
            $phone_numbers = phone_numbers::find($id);
            $phone_numbers->phone_number = request('phone_number');
            $phone_numbers->name = request('name');
            $phone_numbers->save();
            return redirect('/phone');
    }

    public function destroy($id)
    {
        phone_numbers::find($id)->delete();
        return Redirect::back();
    }

}