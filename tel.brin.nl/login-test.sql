-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 22 jan 2019 om 10:56
-- Serverversie: 10.1.36-MariaDB
-- PHP-versie: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login-test`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2018_11_22_104359__create_admin_user', 1),
(27, '2014_10_12_000000_create_users_table', 2),
(28, '2018_11_01_141119_create_phone_numbers_table', 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `phone_numbers`
--

CREATE TABLE `phone_numbers` (
  `id` int(11) NOT NULL,
  `phone_number` char(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `phone_numbers`
--

INSERT INTO `phone_numbers` (`id`, `phone_number`, `name`, `created_at`, `updated_at`) VALUES
(52, '31000000001', 'Test Nummer 1', '2019-01-15 13:23:59', '2019-01-15 14:23:59'),
(53, '31000000002', 'Test Nummer 2', '2019-01-15 13:24:24', '2019-01-15 14:24:24'),
(54, '31000000003', 'Test Nummer 3', '2019-01-15 13:24:46', '2019-01-15 14:24:46'),
(55, '31000000004', 'Test Nummer 4', '2019-01-15 13:25:09', '2019-01-15 14:25:09'),
(58, '31000000005', 'Test Nummer 5', '2019-01-15 13:26:17', '2019-01-15 14:26:17'),
(59, '31000000006', 'Test Nummer 6', '2019-01-15 13:26:35', '2019-01-15 14:26:35'),
(60, '31000000007', 'Test Nummer 7', '2019-01-15 13:26:53', '2019-01-15 14:26:53'),
(61, '31000000008', 'Test Nummer 8', '2019-01-15 13:27:07', '2019-01-15 14:27:07'),
(62, '31000000009', 'Test Nummer 9', '2019-01-15 13:27:27', '2019-01-15 14:27:27'),
(63, '31000000010', 'Test Nummer 10', '2019-01-15 13:27:42', '2019-01-15 14:27:42'),
(64, '31000000011', 'Test Nummer 11', '2019-01-15 13:28:27', '2019-01-15 14:28:27'),
(65, '31000000012', 'Test Nummer 12', '2019-01-15 13:28:51', '2019-01-15 14:28:51'),
(66, '31000000013', 'Test Nummer 13', '2019-01-15 13:29:15', '2019-01-15 14:29:15'),
(67, '31000000014', 'Test Nummer 14', '2019-01-15 13:29:31', '2019-01-15 14:29:31'),
(68, '31000000015', 'Test Nummer 15', '2019-01-15 13:29:45', '2019-01-15 14:29:45'),
(69, '31000000016', 'Test Nummer 16', '2019-01-15 13:30:00', '2019-01-15 14:30:00'),
(70, '31000000017', 'Test Nummer 17', '2019-01-15 13:30:16', '2019-01-15 14:30:16'),
(71, '31999999999', 'Test Nummer 99', '2019-01-15 13:55:06', '2019-01-15 14:55:06'),
(72, '31000000050', 'Test Nummer 50', '2019-01-15 14:01:39', '2019-01-15 15:01:39');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'future@negotica.net', '$2y$10$xDgfEd04w/cSSQ8NWE.kW.Aq7cQedzp8shSIi7H7NYlOkRh9EaUZi', 'aU0Kxv4WtkQTtlYVcCxkZmNG685lw6JEU59eieHImkWgdbgcvk1dGdAe8Khy', '2018-11-26 08:00:10', NULL);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `phone_numbers`
--
ALTER TABLE `phone_numbers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_numbers_phone_number_unique` (`phone_number`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT voor een tabel `phone_numbers`
--
ALTER TABLE `phone_numbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
