@extends('layouts.app')
@section('title')

    Telefoonnummers

@endsection
@section('content')

<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Telefoonnummer toevoegen</h4>
                    <p>Alle nummers moeten beginnen met 31 en mogen geen letters of andere tekens bevatten</p>
                        <form action="{{action('PhoneController@store')}}" method="POST" style="margin: 0;">
                            @csrf
                            <input id="add_phone_number" type="text" name="phone_number" placeholder="Telefoonnummer">
                            <input id="add_name" type="text" name="name" placeholder="Naam">
                            <input class="btn btn-primary" type="submit" name="phone" value="Toevoegen" style="cursor: pointer;">
                        </form>
                        </div>
                            <div class="notification">
                                <ul id="error_message">
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                                <table id="number-user-table">
                                    <tr>
                                        <th id="phone_number_row">Telefoonnummer</th>
                                        <th id="name_row">Naam</th>
                                        <th id="change_row_aanpassen">Aanpassen</th>
                                    </tr>
                                        @foreach ($phone_numbers as $number)
                                    <tr>
                                        <td> {{ $number->phone_number }}</td>
                                        <td> {{ $number->name }}</td>
                                        <td>
                                            <form action="/phone/{{ $number->id }}" method="POST">
                                                @csrf @method('DELETE')
                                                <button id="but_delete" type="submit" class="btn btn-danger"
                                                        onclick="return confirm('Weet je zeker dat je dit nummer wil verwijderen?');">
                                                        <i class="far fa-trash-alt"> </i>
                                                </button>
                                            </form>
                                            <form action="/phone/{{ $number->id }}/edit" method="GET">
                                                @csrf
                                                <button id="but_change" type="submit" class="btn btn-secondary">
                                                    <i class="far fa-edit"> </i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                        {{ $phone_numbers->render() }}
                    </div>
                </div>
            </div>
        </div>
@endsection