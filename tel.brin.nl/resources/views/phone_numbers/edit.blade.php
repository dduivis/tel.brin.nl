@extends('layouts.app')
@section('title')

    Telefoonnummer aanpassen

@endsection
@section('content')
    <head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                       <h5>Telefoonnummer Aanpassen</h5>
                        <p>Alle nummers moeten beginnen met 31 en mogen geen letters of andere tekens bevatten</p>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="/phone/{{ $phone_numbers->id }}">
                            @method('PATCH')
                            @csrf
                            <div class="field">
                                <label class="label" for="phone_number">Telefoonnummer</label>
                                     <div class="control">
                                         <input type="text" class="input" name="phone_number" placeholder="Telefoonnummer"
                                                value="{{ $phone_numbers->phone_number }}"><br><br>
                                     </div>
                            </div>
                            <div class="field">
                                <label class="label" for="name">Naam</label>
                                     <div class="control">
                                          <input type="text" class="input" name="name" value="{{ $phone_numbers->name }}" >
                                     </div>
                            </div>
                                <br>
                            <div class="field">
                                <div class="control">
                                  <form>
                                      @csrf
                                      <button type="submit" class="btn btn-primary"> Opslaan </button>
                                  </form>
                                    <button onclick="window.location.href='../../'" type="reset" class="btn btn-danger"> Annuleer </button>
                                </div>
                            </div>
                        </form>
                            <div class="notification is-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>
                                            {{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection