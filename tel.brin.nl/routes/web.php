<?php
use App\Http\Middleware\RedirectIfAuthenticated;
use App\phone_numbers;
use Faker\Provider\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('phone');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/B20QUY9YPGD21B0G/lookup/', function(\Illuminate\Http\Request $request)
{
    {
        $input = phone_numbers::where('phone_number', '=', $request['cid'])->first();

        if($input === null){
            die ('0');
        }
        else{
            echo '1';
        }
}});

Auth::routes();

Route::resource('phone', 'PhoneController');






